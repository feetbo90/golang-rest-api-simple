package config

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/mhdiqbalpradipta/restapi-go/structs"
)

// DBInit create connection to database
func DBInit() *gorm.DB {
	db, err := gorm.Open("mysql", "root:iqbalganteng@tcp(localhost:32775)/restapi?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println(err)
		panic("failed to connect to database")
	}

	db.AutoMigrate(structs.Person{})
	return db
}
