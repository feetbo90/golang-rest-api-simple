package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/mhdiqbalpradipta/restapi-go/config"
	"github.com/mhdiqbalpradipta/restapi-go/controllers"
)

func main() {
	db := config.DBInit()
	inDB := &controllers.InDB{DB: db}

	router := gin.Default()
	router.GET("/get-person/:id", inDB.GetPersons)
	router.POST("/person", inDB.CreatePerson)
	router.PUT("/update-person", inDB.UpdatePerson)
	router.DELETE("/delete-person/:id", inDB.DeletePerson)
	router.Run(":3001")
}
